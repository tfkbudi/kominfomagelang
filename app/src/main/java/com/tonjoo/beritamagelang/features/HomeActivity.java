package com.tonjoo.beritamagelang.features;

import android.app.SearchManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.tonjoo.beritamagelang.R;
import com.tonjoo.beritamagelang.adapter.NewsAdapter;
import com.tonjoo.beritamagelang.model.ModelNews;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.navigation_view)
    NavigationView navigationView;

    @BindView(R.id.rv_news)
    RecyclerView rvNews;

    ActionBar actionBar;
    List<ModelNews> listNews = new ArrayList<>();
    NewsAdapter adapter;
    SearchView searchView;
    private MenuItem searchMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        dummy();
        init();

    }

    private void init() {
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionBar.setDisplayHomeAsUpEnabled(true);

        navigationView.setNavigationItemSelectedListener(this);

        loadData();
    }

    public void onResume() {
        super.onResume();
        Log.e("debug", "onResume");
    }

    private void loadData() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvNews.setLayoutManager(mLayoutManager);
        rvNews.setItemAnimator(new DefaultItemAnimator());

        updateData();
    }

    private void updateData(){
        adapter = new NewsAdapter(this, listNews);
        rvNews.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);

        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);

        searchMenuItem = menu.findItem(R.id.search);
        searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);

        searchView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View view) {

            }

            @Override
            public void onViewDetachedFromWindow(View view) {
                //onclose search
                listNews.clear();
                dummy();
                updateData();

            }
        });

        return true;
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawerLayout.closeDrawers();
        switch (item.getItemId()) {

        }
        return true;
    }

    private void dummy() {
        for (int i = 0; i < 5; i++) {
            ModelNews model = new ModelNews();
            model.setTitle("BNPB AKAN GELAR SEKOLAH GUNUNG DI DESA NGARGORETNO");
            model.setDate("07 Oktober 2017");
            model.setThumbnail("http://diskominfo.magelangkab.go.id/images/30036bcdf1ed6ed1bb84d985fb27d194.jpeg");
            model.setContent("<p>Pada kegiatan" +
                    "sosialisasi sekolah gunung yang diikuti oleh relawan, komunitas, dunia usaha," +
                    "akademisi dan aparatur tersebut dipaparkan tentang Mengenal Bahaya Geologi dan" +
                    "Hidrometerologi oleh Helmi Murwanto dari UPN Jogjakarta, Pengelolaan DAS Terpadu" +
                    "oleh Sri Lestari dari BPDASHLSOP, Dinas LH Kabupaten Magelang dan TWCB Borobudur." +
                    "(Diskominfo)</p>\n");

            listNews.add(model);

        }
    }

    private void dummy2() {
        ModelNews model = new ModelNews();
        model.setTitle("BNPB AKAN GELAR SEKOLAH GUNUNG DI DESA NGARGORETNO");
        model.setDate("07 Oktober 2017");
        model.setThumbnail("http://diskominfo.magelangkab.go.id/images/30036bcdf1ed6ed1bb84d985fb27d194.jpeg");
        model.setContent("<p>Pada kegiatan" +
                "sosialisasi sekolah gunung yang diikuti oleh relawan, komunitas, dunia usaha," +
                "akademisi dan aparatur tersebut dipaparkan tentang Mengenal Bahaya Geologi dan" +
                "Hidrometerologi oleh Helmi Murwanto dari UPN Jogjakarta, Pengelolaan DAS Terpadu" +
                "oleh Sri Lestari dari BPDASHLSOP, Dinas LH Kabupaten Magelang dan TWCB Borobudur." +
                "(Diskominfo)</p>\n");

        listNews.add(model);


    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if(newText.length() > 0){
            listNews.clear();
            dummy2();
            updateData();
        }else{
            listNews.clear();
            dummy();
            updateData();
        }

        return true;
    }
}
