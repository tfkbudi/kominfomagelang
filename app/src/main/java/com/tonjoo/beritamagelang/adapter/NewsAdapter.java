package com.tonjoo.beritamagelang.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tonjoo.beritamagelang.R;
import com.tonjoo.beritamagelang.features.DetailActivity;
import com.tonjoo.beritamagelang.model.ModelNews;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TFKBUDI on 07/11/2017.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder>{

    private List<ModelNews> listNews;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tv_title)
        TextView tvTitle;

        @BindView(R.id.tv_date)
        TextView tvDate;

        @BindView(R.id.img_thumb)
        ImageView imgThumb;

        @BindView(R.id.cardview)
        CardView cvParent;

        public MyViewHolder(View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    public NewsAdapter(Context context, List<ModelNews> listNewa){
        this.context = context;
        this.listNews = listNewa;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_news, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ModelNews news = listNews.get(position);

        holder.tvTitle.setText(news.getTitle());
        holder.tvDate.setText(news.getDate());

        Glide.with(context).load(news.getThumbnail()).fitCenter().into(holder.imgThumb);

        holder.cvParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, DetailActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listNews.size();
    }

}
