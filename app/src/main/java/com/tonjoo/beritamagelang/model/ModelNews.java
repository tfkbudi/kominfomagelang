package com.tonjoo.beritamagelang.model;

/**
 * Created by TFKBUDI on 07/11/2017.
 */

public class ModelNews {
    public String title;
    public String date;
    public String thumbnail;
    public String content;

    public ModelNews(){

    }

    public ModelNews(String title, String date, String thumbnail, String content) {
        this.title = title;
        this.date = date;
        this.thumbnail = thumbnail;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
