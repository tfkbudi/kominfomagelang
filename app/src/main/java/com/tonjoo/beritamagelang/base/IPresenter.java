package com.tonjoo.beritamagelang.base;

/**
 * Created by iast on 8/7/17.
 */

public interface IPresenter <V extends IView>{

    void attachView(V iView);

    void detachView();
}
