package com.tonjoo.beritamagelang.base;

/**
 * Created by iast on 8/11/17.
 */

public class BasePresenter<T extends IView> implements IPresenter<T> {
    private T iView;

    @Override
    public void attachView(T iView) {
        this.iView = iView;
    }

    @Override
    public void detachView() {
        iView = null;
    }

    public boolean isViewAttached() {
        return iView != null;
    }

    public T getIView() {
        return iView;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new IViewNotAttachedException();
    }

    public static class IViewNotAttachedException extends RuntimeException {
        public IViewNotAttachedException() {
            super(
                    "Please call Presenter.attachView(IView) before" + " requesting data to the Presenter");
        }
    }
}
