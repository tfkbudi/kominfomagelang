package com.tonjoo.beritamagelang.base;

/**
 * Created by iast on 8/7/17.
 */

public interface IView {
    //Fill this class with methods that mostly used in every activity, ex: void showLoading();

    void showLoading();

    void hideLoading();
}
